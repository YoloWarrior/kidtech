import React from "react";
import Catalog from "./pages/Catalog";
import { Provider } from "react-redux";
import { setupStore } from "./store/store";

const store = setupStore();

function App() {
  return (
    <Provider store={store}>
      <Catalog />
    </Provider>
  );
}

export default App;
