import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { createSelector } from "reselect";
import { Course } from "../../interfaces/Course";
import { RootState } from "../store";

interface CourseState {
  courses: Course[];
  tags: string[];
}

const initialState: CourseState = {
  courses: [],
  tags: [],
};

const coursesState = (state: RootState) => state.courseReducer;

const courselice = createSlice({
  name: "course",
  initialState,
  reducers: {
    setCourses: (state, action: PayloadAction<Course[]>) => {
      state.courses = action.payload;
    },
    setTags: (state, action: PayloadAction<string[]>) => {
      state.tags = action.payload;
    },
  },
});

export const selectFilteredCourses = createSelector(
  coursesState,
  (_: RootState, selectedTag: string) => selectedTag,
  (courseState: CourseState, selectedTag: string) => {
    if (!selectedTag) {
      return courseState.courses;
    }
    return courseState.courses.filter((course) =>
      course.tags.includes(selectedTag)
    );
  }
);

export const { setCourses, setTags } = courselice.actions;

export default courselice.reducer;
