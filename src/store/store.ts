import { combineReducers, configureStore } from "@reduxjs/toolkit";
import courseReducer from "./reducers/CourseSlice";
import { catalogApi } from "../services/CatalogService";

const rootReducer = combineReducers({
  courseReducer,
  [catalogApi.reducerPath]: catalogApi.reducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({ serializableCheck: false }).concat(
        catalogApi.middleware
      ),
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];
