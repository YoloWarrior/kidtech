import { createApi } from "@reduxjs/toolkit/query/react";
import courses from "../assets/courses.json";
import { Course } from "../interfaces/Course";

export const catalogApi = createApi({
  reducerPath: "catalogApi",
  baseQuery: async () => ({
    data: courses,
  }),
  endpoints: (builder) => ({
    getCourses: builder.query<Course[], void>({
      query: () => {
        return courses;
      },
    }),
  }),
});

export const { useLazyGetCoursesQuery } = catalogApi;

export const { endpoints, reducerPath, reducer } = catalogApi;
