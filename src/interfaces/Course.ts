export interface Course {
  id: string;
  name: string;
  bgColor: string;
  image: string;
  tags: string[];
}
