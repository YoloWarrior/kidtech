import React, { useEffect, useState } from "react";
import { useLazyGetCoursesQuery } from "../services/CatalogService";
import { useAppDispatch, useAppSelector } from "../hooks/useRedux";
import Courses from "../components/Courses";
import Tags from "../components/Tags";
import {
  setTags,
  setCourses,
  selectFilteredCourses,
} from "../store/reducers/CourseSlice";

const Catalog = () => {
  const [getCourses] = useLazyGetCoursesQuery();
  const dispatch = useAppDispatch();
  const [selectedTag, setSelectedTag] = useState("");
  const tags = useAppSelector((state) => state.courseReducer.tags);
  const courses = useAppSelector((state) =>
    selectFilteredCourses(state, selectedTag)
  );

  useEffect(() => {
    const fetchData = async () => {
      const { isSuccess, data } = await getCourses();
      if (isSuccess) {
        const tags = new Set(data?.flatMap((course) => course.tags));

        dispatch(setTags([...tags]));
        dispatch(setCourses(data));
      }
    };

    fetchData();
  }, []);

  return (
    <div className="page catalog">
      <Tags items={tags} selected={selectedTag} onSelect={setSelectedTag} />
      <Courses courses={courses} />
    </div>
  );
};

export default Catalog;
