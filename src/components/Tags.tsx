import React from "react";

interface TagsProps {
  items: string[];
  onSelect: (selected: string) => void;
  selected: string;
}

const Tags = ({ items, selected, onSelect }: TagsProps) => {
  return (
    <div className="tags">
      {items.map((tag) => (
        <span
          onClick={() => onSelect(tag)}
          className={`tags__item ${selected === tag ? "selected" : ""}`}
          key={tag}
        >
          {tag}
        </span>
      ))}
    </div>
  );
};

export default Tags;
