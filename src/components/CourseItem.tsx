import React from "react";
import { Course } from "../interfaces/Course";

const CourseItem = ({ bgColor, name, image }: Course) => {
  return (
    <div className="courses__item" style={{ backgroundColor: bgColor }}>
      <img className="courses__item-image" src={image} alt="img" />
      <div className="courses__item-info">
        <span className="courses__item-name">{name}</span>
      </div>
    </div>
  );
};

export default CourseItem;
