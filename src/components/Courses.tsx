import React from "react";
import CourseItem from "./CourseItem";
import { Course } from "../interfaces/Course";

interface CoursesProps {
  courses: Course[];
}

const Courses = ({ courses }: CoursesProps) => {
  return (
    <div className="courses">
      {courses.map((course) => (
        <CourseItem key={course.id} {...course} />
      ))}
    </div>
  );
};

export default Courses;
